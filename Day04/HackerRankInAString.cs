﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class HackerRankInAString
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Total Inputan :");
            int TotalInputan = int.Parse(Console.ReadLine()); //Ini itu untuk masukan berapa kali inputan
            string[] Inputan = new string[TotalInputan]; // ini itu untuk nampung inputannya kedalam array
            // new string[TotalInputan] artinya array Inputan ada Sebanyak Total Inputan X

            Console.WriteLine("Masukkan Kata Pembanding");
            string kataPembanding = Console.ReadLine();

            for (int i = 0; i < TotalInputan; i++)
            {
                Console.WriteLine("Masukan Inputan Ke-" + Convert.ToInt32(i + 1));
                Inputan[i] = Console.ReadLine().ToLower(); //nah tadi kenapa aku buat dalamarray
                //soalnya inputannya dsni kan gtw berapa kali. jadi dimasukan aja ke dalam array berapa banyak inputannya

            }

            //string pengecekan = "hackerrank";
            char[] arraypengecekan = kataPembanding.ToCharArray();
            for (int i = 0; i < Inputan.Length; i++)
            {
                char[] array = Inputan[i].ToCharArray();
                int PertambahanArrayPengecekan = 0;
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[j] == arraypengecekan[PertambahanArrayPengecekan])
                    {
                        PertambahanArrayPengecekan++;
                    }

                }

                if (PertambahanArrayPengecekan != arraypengecekan.Length)
                {
                    Console.WriteLine("Jawaban Kalimat Ke-" + Convert.ToInt32(i + 1) + " Adalah NO");
                }
                else
                {
                    Console.WriteLine("Jawaban Kalimat Ke-" + Convert.ToInt32(i + 1) + " Adalah YES");
                }
            }


            Console.ReadKey();
        }
    }
}
