﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MiddleAsterisk01
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the alphabet");
            string sentence = Console.ReadLine();

            string[] sentencesArray = sentence.Split(' ');

            for (int i = 0; i < sentencesArray.Length; i++)
            {
                if (sentencesArray[i].Length < 3 )
                {
                    Console.Write(" ");
                }
                else
                {
                    Console.Write(sentencesArray[i].First() + "***" + sentencesArray[i].Last() + ' ');
                }
            }
            Console.WriteLine();
        }
    }
}
