﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Middle Asterisk 01");
                        MiddleAsterisk01.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("Palindrome");
                        Palindrome.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("Camel Case");
                        CamelCase.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("Strong Password");
                        StrongPassword.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("Caesar Chipper");
                        CaesarChiper.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("Mars Exploration");
                        MarsExploration.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("Hacker Rank in A string");
                        HackerRankInAString.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("Pangrams");
                        Pangrams.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("Separate The Numbers");
                        SeparateTheNumbers.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("Gemstone");
                        Gemstone.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("Making Anagram");
                        MakingAnagram.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("Two String");
                        TwoString.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
