﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Palindrome
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the alphabet");
            string alpha = Console.ReadLine();
            string alpha1 = alpha;

            char[] charArray = alpha.ToCharArray();
            Array.Reverse(charArray);
            string alpha2 = new string(charArray);
            string result = "";

                if (alpha2 == alpha)
                {
                    result = "True";
                }
                else
                {
                    result = "False";
                }

            Console.WriteLine("Result is  " + result);
        }
    }
}
