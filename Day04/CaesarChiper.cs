﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarChiper
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the length of alphabet");

            int length, shift;

            length = Convert.ToInt32(Console.ReadLine());
            string input = Console.ReadLine();
            shift = Convert.ToInt32(Console.ReadLine());

            int[] alphabet = new int[length];
            char[] rotate = new char[length];
            for (int i = 0; i < length; i++)
            {
                alphabet[i] = (int)input[i];
                if (alphabet[i] > 64 && alphabet[i] < 91)
                {
                    alphabet[i] = (65 + ((alphabet[i] - 65) + shift) % 26);
                }
                else if (alphabet[i] > 96 && alphabet[i] < 123)
                {
                    alphabet[i] = (97 + ((alphabet[i] - 97) + shift) % 26);
                }  
                rotate[i] = (char)alphabet[i];
                Console.Write(rotate[i]);
            }
            Console.WriteLine();
       

        //    Console.WriteLine("Enter the caesar chiper");
        //    string caesarCipher = Console.ReadLine();

        //    Console.WriteLine("Enter the rotated value");
        //    int rotated = int.Parse(Console.ReadLine());

        //    char[] arrayCaesarChiper = caesarCipher.ToCharArray();
        //    string result = "";

        //    for (int i = 0; i < arrayCaesarChiper.Length; i++)
        //    {
        //        result += cekHuruf(arrayCaesarChiper[i], rotated);
        //    }

        //    Console.WriteLine();
        //    Console.ReadKey();

        //}

        //static char cekHuruf (char perAlphabet, int rotated)
        //{
        //    char result;
        //    if (char.IsLetterOrDigit(perAlphabet))
        //    {
        //        return perAlphabet;
        //    }

        //    //Prinsip kita itu gini
        //    //Tau Char?
        //    //a itu selalu 97 (ini kata pertama) <- Pembentukan karaketer
        //    //klo a ke c itu gampang tinggal di tambah rotated
        //    //z itu kenapa 122 (logikanyaklo a itu 26) berarti z itu (26-1) + 97 = 122
        //    //122+2 = 124
        //    //124 - 97 = 27
        //    //27%26 = 1
        //    // 97 + 1 = 98
        //    // 98 itu b

        //    char hurufPertama = char.IsUpper(perAlphabet) ? 'A' : 'a';
        //    int selisihHurufDanRotated = perAlphabet + rotated;
        //    int diKurangHurufPertama = selisihHurufDanRotated - hurufPertama;
        //    int selisihKaraketer = diKurangHurufPertama % 26; //26 itu karena total huruf ad 26
        //    int hasilSetelahDitambah = selisihKaraketer + hurufPertama;
        //    result = (char)hasilSetelahDitambah;
        //    return result;
        }
    }
}

