﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Gemstone
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Berapa Banyak Inputan");
            string sentence = Console.ReadLine();
            int length = Convert.ToInt32(sentence);

            string[] stringArray = new string[length];

            int lengthInput = 0;
            while (lengthInput < length)
            {
                Console.WriteLine("Masukan set huruf " + (lengthInput + 1));
                string inputString = Console.ReadLine();

                stringArray[lengthInput] = inputString.ToLower();

                lengthInput++;
            }

            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            int temp = 0;
            int count = 0;

            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < stringArray.Length; j++)
                {
                    if (stringArray[j].Contains(alphabet[i]))
                    {
                        temp++;
                    }
                }

                if (temp == stringArray.Length)
                {
                    count++;
                }
                temp = 0;
            }
            Console.WriteLine(count);
        }
    }
}
