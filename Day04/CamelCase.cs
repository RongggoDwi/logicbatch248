﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CamelCase
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Kalimat");
            string kalimat = Console.ReadLine();

            char[] kalimatChar = kalimat.ToCharArray();
            int total = 1;

            // Jika awal kata huruf kecil
            if (char.IsLower(kalimatChar[0]))
            {
                for (int i = 0; i < kalimatChar.Length; i++)
                {
                    if (char.IsUpper(kalimatChar[i]))
                    {
                        total += 1;
                    }
                }
                Console.WriteLine(total);
            }
            // jika awal kata huruf besar
            else
            {
                Console.WriteLine("Bukan Camel Case");
            } 
        }
    }
}
