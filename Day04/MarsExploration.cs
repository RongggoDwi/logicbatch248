﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MarsExploration
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan kata : ");
            string sos = Console.ReadLine().ToUpper();

            char[] sosChar = sos.ToCharArray();
            string SOS = "SOS";

            char[] ArraySOS = SOS.ToCharArray();
            int difference = 0;

            if (sosChar.Length % 3 != 0)
            {
                Console.WriteLine("Tidak Sesuai Format");
            }
            else
            {
                for (int i = 0; i < sosChar.Length; i++)
                {
                    if (sosChar[i] != ArraySOS[i % 3])
                    {
                        difference += 1;
                    }
                }
                Console.WriteLine("Perbedaan Sinyal: " + difference);
            }

        }
    }
}
