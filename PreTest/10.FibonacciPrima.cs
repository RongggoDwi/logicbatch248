﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Soal10
    {
        public static void Resolve()
        {

            Console.WriteLine("Masukkan panjang baris angka");
            int panjang = int.Parse(Console.ReadLine());

            int[] fibbonacciArray = new int[panjang];
            Console.WriteLine("Baris Fibonacci ");
            for (int i = 0; i < panjang; i++)
            {
                if (i <= 1)
                {
                    fibbonacciArray[i] = 1;
                }
                else
                {
                    fibbonacciArray[i] = fibbonacciArray[i - 1] + fibbonacciArray[i - 2];
                }
                Console.Write(fibbonacciArray[i] + " ");
            }
            Console.WriteLine();


            int bilanganPrima = 2;
            int panjangBilanganPrima = 0;
            int[] arrayBilanganPrima = new int[panjang];
            Console.WriteLine("Baris Bilangan Prima ");
            while (panjangBilanganPrima < panjang)
            {
                int faktorBilanganPrima = 0;

                for (int i = 1; i <= bilanganPrima; i++)
                {
                    if (bilanganPrima % i == 0)
                    {
                        faktorBilanganPrima += 1;
                    }
                }

                if (faktorBilanganPrima == 2)
                {
                    arrayBilanganPrima[panjangBilanganPrima] = bilanganPrima;
                    Console.Write(bilanganPrima + " ");
                    panjangBilanganPrima++;
                }
                bilanganPrima++;
            }

            Console.WriteLine();

            int[] arrayAkhir = new int[panjang];
            Console.WriteLine("Hasil Akhir ");
            for (int i = 0; i < panjang; i++)
            {
                arrayAkhir[i] = arrayBilanganPrima[i] + fibbonacciArray[i];
                Console.Write(arrayAkhir[i] + " ");

            }
            Console.WriteLine();
        }
    }
}
