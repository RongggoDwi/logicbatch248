﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Soal09
    {
        public static void Resolve()
        {
            Console.WriteLine("Jumlah Kartu :");
            int jumlahKartu = int.Parse(Console.ReadLine());

            Console.WriteLine("Jumlah Tawaran");
            int jumlahTawaran = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the score of kotakA");
            string kotakA = Console.ReadLine();

            int[] kotakAarray = Utility.ConvertStringToIntArray(kotakA);
            int totalKotakA = 0;

            Console.WriteLine("Enter the score of kotakB");
            string kotakB = Console.ReadLine();

            int[] bobArray = Utility.ConvertStringToIntArray(kotakB);
            int totalKotakB = 0;

            if (kotakAarray.Length != bobArray.Length)
            {
                Console.WriteLine("Format yang anda masukkan salah");
            }
            else
            {
                for (int i = 0; i < kotakAarray.Length; i++)
                {
                    if (kotakAarray[i] > bobArray[i])
                    {
                        totalKotakA += 1;
                    }
                    else if (kotakAarray[i] < bobArray[i])
                    {
                        totalKotakB += 1;
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Hasil Total Kotak A : " + totalKotakA);
                Console.WriteLine("Hasil Total Kotak B : " + totalKotakB);
            }

        }
    }
}
