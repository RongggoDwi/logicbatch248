﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                string answer = "Y";

                while (answer.ToUpper() == "Y")
                {
                    Console.WriteLine("Masukan nomor soal");
                    int nomorSoal = int.Parse(Console.ReadLine());

                    switch (nomorSoal)
                    {
                        case 1:
                            Console.WriteLine("Soal 01");
                            Soal01.Resolve();
                            break;
                        case 2:
                            Console.WriteLine("Soal 02");
                            Soal02.Resolve();
                            break;
                        case 3:
                            Console.WriteLine("Soal 03");
                            Soal03.Resolve();
                            break;
                        case 6:
                            Console.WriteLine("Soal 06");
                            Soal06.Resolve();
                            break;
                        case 7:
                            Console.WriteLine("Soal 07");
                            Soal07.Resolve();
                            break;
                        case 8:
                            Console.WriteLine("Soal 08");
                            Soal08.Resolve();
                            break;
                        case 9:
                            Console.WriteLine("Soal 09");
                            Soal09.Resolve();
                            break;
                        case 10:
                            Console.WriteLine("Soal 10");
                            Soal10.Resolve();
                            break;
                        default:
                            Console.WriteLine("nomorSoal tidak ditemukan");
                            break;
                    }

                    Console.WriteLine("Lanjutkan?");
                    answer = Console.ReadLine();
                    Console.WriteLine();
                }
            }
        }
    }
}
