﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Soal01
    {
        public static void Resolve()
        {
            Console.WriteLine("Beli Berapa Pulsa");
            string pulsa = Console.ReadLine();

            int[] pulsaArray = Utility.ConvertStringToIntArray(pulsa);
            int point = 0;
            //int totalPoint = 0;

            for (int i = 0; i < pulsaArray.Length; i++)
            {
                if (pulsaArray[i] <= 10000)
                {
                    point = 0;
                }
                else if (pulsaArray[i] <= 20000)
                {
                    point = 10000 / 1000;
                }
                else if (pulsaArray[i] <= 30000)
                {
                    point = 20000 / 1000;
                }
                else if (pulsaArray[i] < 45000)
                {
                    point = 45000 / 1000 * 2;
                }
                else if (pulsaArray[i] <= 75000)
                {
                    point = (45000 / 1000) * 2;
                    point = point + 20;
                }
                Console.WriteLine("Total Point :" + point);
            }

        }
    }
}
