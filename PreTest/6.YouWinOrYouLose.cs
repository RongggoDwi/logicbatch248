﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Soal06
    {
        public static void Resolve()
        {
            Console.WriteLine("Silahkan pilih siapa yang main dulu : 1. Player || 2. Komputer");
            int answer = int.Parse(Console.ReadLine());

            if (answer == 1)
            {
                Console.WriteLine("Silahkan Player Anda Duluan");

                int pemain = int.Parse(Console.ReadLine());

                if (pemain > 9 || pemain < 0)
                {
                    Console.WriteLine("Out of range");
                }

                else if (answer == 2)
                {
                    Random komputer = new Random();
                    int angkaKomputer = komputer.Next(0, 9);

                    Console.WriteLine("Komputer giliran anda");
                    Console.WriteLine(angkaKomputer);

                    if (pemain > angkaKomputer)
                    {
                        Console.WriteLine("You Win");
                    }
                    else
                    {
                        Console.WriteLine("You Lose");
                    }
                }
            }

            if (answer == 2)
            {
                Console.WriteLine("Silahkan Komputer Anda Duluan");

                Random komputer = new Random();
                int angkaKomputer = komputer.Next(0, 9);

                Console.WriteLine("Komputer giliran anda");
                Console.WriteLine(angkaKomputer);

                if (angkaKomputer > 9 || angkaKomputer < 0)
                {
                    Console.WriteLine("Out of range");
                }

                else
                {
                    int pemain = int.Parse(Console.ReadLine());

                    if (angkaKomputer > pemain)
                    {
                        Console.WriteLine("You Win");
                    }
                    else
                    {
                        Console.WriteLine("You Lose");
                    }
                }
            }
            
        }
    }
}
