﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Soal07
    {
        public static void Resolve()
        {
            Console.WriteLine("Keranjang 1 :");
            string keranjang1 = Console.ReadLine().ToLower();

            Console.WriteLine("Keranjang 2 :");
            string keranjang2 = Console.ReadLine().ToLower();

            Console.WriteLine("Keranjang 3 :");
            string keranjang3 = Console.ReadLine().ToLower();

            Random komputer = new Random();
            int keranjangPasar = komputer.Next(1, 3);
            int temp = 0;

            string[] keranjangArray = new string[] { keranjang1, keranjang2, keranjang3 };

            if (keranjang1 == "kosong")
            {
                keranjang1 = "0";
            }
            if (keranjang2 == "kosong")
            {
                keranjang1 = "0";
            }
            if (keranjang3 == "kosong")
            {
                keranjang1 = "0";
            }


            if (keranjangPasar == 1)
            {
                temp = int.Parse(keranjang2) + int.Parse(keranjang3);
            }

            else if (keranjangPasar == 2)
            {
                temp = int.Parse(keranjang1) + int.Parse(keranjang3);
            }

            else if (keranjangPasar == 3)
            {
                temp = int.Parse(keranjang1) + int.Parse(keranjang2);
            }


            Console.WriteLine("Keranjang yang dibawa kepasar " + keranjangPasar);
            Console.WriteLine("Total buah yang ada di dapur " + temp);
        }
    }
}
