﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PreTest
{
    class Class1
    {
        public static void Resolve()
        {
            Console.WriteLine("Laki-laki Dewasa :");
            double lakilaki = double.Parse(Console.ReadLine());

            Console.WriteLine("Perempuan Dewas :");
            double perempuan = double.Parse(Console.ReadLine());

            Console.WriteLine("Anak-anak :");
            double anakanak = double.Parse(Console.ReadLine());

            Console.WriteLine("balita :");
            double balita = double.Parse(Console.ReadLine());

            Console.WriteLine("Masih ada tambahan tidak : (Y/N)");
            string answer = (Console.ReadLine().ToUpper());

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("1. laki-laki || 2. Anak-anak || 3.Perempuan || 4.Balita");
                string tambahan = Console.ReadLine();
                Console.WriteLine();

                if ("1" == tambahan)
                {
                    Console.WriteLine("Nambah berapa: ");
                    double tambahPorsi = double.Parse(Console.ReadLine());
                    lakilaki += tambahPorsi;
                }

                else if ("2" == tambahan)
                {
                    Console.WriteLine("Nambah berapa: ");
                    double tambahPorsi = double.Parse(Console.ReadLine());
                    perempuan += tambahPorsi;
                }

                else if ("3" == tambahan)
                {
                    Console.WriteLine("Nambah berapa: ");
                    double tambahPorsi = double.Parse(Console.ReadLine());
                    anakanak += tambahPorsi;
                }

                else if ("4" == tambahan)
                {
                    Console.WriteLine("Nambah berapa: ");
                    double tambahPorsi = double.Parse(Console.ReadLine());
                    balita += tambahPorsi;
                }

                Console.WriteLine("Masih ada tambahan tidak kakak : (Y/N)");
                answer = (Console.ReadLine().ToUpper());
                Console.WriteLine();
            }

            lakilaki = lakilaki * 2;
            perempuan = perempuan * 1;
            balita = balita * 1;
            anakanak = anakanak * 1 / 2;

            double totalPorsi = lakilaki + perempuan + balita + anakanak;

            Console.WriteLine(totalPorsi + " Porsi");
        }
    }
}
