﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Pustaka
    {
        public static void Resolve()
        {
            Console.WriteLine("Berapa Buku yang dipinjam : ");
            int buku = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan Tanggal Peminjaman : ");
            string timePeminjaman = Console.ReadLine();

            Console.WriteLine("Masukkan Tanggal Pengembalian : ");
            string timePengembalian = Console.ReadLine();

            DateTime waktuPeminjaman = Convert.ToDateTime(timePeminjaman);

            DateTime waktuPengembalian = Convert.ToDateTime(timePengembalian);

            int lamaPeminjaman = (waktuPengembalian - waktuPeminjaman).Days;

            int totalDenda = (buku * lamaPeminjaman) * 5000 - (15000 * buku);

            if (lamaPeminjaman > 3)
            {
                Console.WriteLine(totalDenda);
            }
            else if(lamaPeminjaman == 0)
            {
                Console.WriteLine("Salah Input");
            }
            else
            {
                Console.WriteLine(" ");
            }

            Console.WriteLine("Jumlah denda yang dibayarkan adalah Rp. " + totalDenda);

            Console.WriteLine();
        }
    }
}
