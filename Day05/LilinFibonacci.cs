﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class LilinFibonacci
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set number");
            string number = Console.ReadLine();

            if (number.Contains('0') || number.Contains('-'))
            {
                Console.WriteLine("Inputan salah");
            }
            else
            {
                int[] numberArray = Utility.ConvertStringToIntArray(number);

                int[] fibonacciArray = new int[numberArray.Length];


                for (int i = 0; i < numberArray.Length; i++)
                {

                    if (i <= 1)
                    {
                        fibonacciArray[i] = 1;
                        Console.Write(fibonacciArray[i] + " ");
                    }
                    else
                    {
                        fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        Console.Write(fibonacciArray[i] + " ");
                    }
                }

                for (int i = 0; i < numberArray.Length; i++)
                {
                    if (numberArray[i] == 1) //karena 1 akan paling cepat habis
                    {
                        Console.WriteLine("Lilin yang habis adalah lilin ke - " + (i + 1));
                        break;
                    }

                    else if (fibonacciArray[i] > 1 && numberArray[i] % fibonacciArray[i] == 0) // kalo fibonaci > 1 karena kalo di modulusin hasilnya 0 jd fibonaci 1 itu harus di skip
                    {
                        Console.WriteLine("Lilin yang habis adalah lilin ke - " + (i + 1));
                        break;
                    }
                }
            }

        }
    }
}
