﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Parkir
    {
        public static void Resolve()
        {
            Console.WriteLine("Masuk Parkir: ");
            string masukParkir = Console.ReadLine();
            Console.WriteLine();

            string[] splitMasuk = masukParkir.Split(':');
            int jamMasuk = Convert.ToInt32(splitMasuk[0]);
            int menitMasuk = Convert.ToInt32(splitMasuk[1]);
            int detikMasuk = Convert.ToInt32(splitMasuk[2]);

            Console.WriteLine("Keluar Parkir: ");
            string keluarParkir = Console.ReadLine();
            Console.WriteLine();

            string[] splitKeluark = keluarParkir.Split(':');
            int jamKeluar = Convert.ToInt32(splitKeluark[0]);
            int menitKeluar = Convert.ToInt32(splitKeluark[1]);
            int detikKeluar = Convert.ToInt32(splitKeluark[2]);

            int biayaParkir = 4000;

            int selisihJam = jamKeluar - jamMasuk;

            if (jamMasuk > 24 || menitMasuk > 60 || detikMasuk > 60 || jamKeluar > 24 || menitKeluar > 60 || detikKeluar > 60)
            {
                Console.WriteLine("Maaf Format Waktu Salah");
            }
            else
            {
                if (selisihJam == 0)
                {
                    Console.WriteLine("Biaya Parkir anda Gratis");
                }
                else if (selisihJam == 1 && menitMasuk >= menitKeluar && detikMasuk >= detikKeluar)
                {
                    Console.WriteLine("Biaya Parkir anda Gratis");
                }
                else if (selisihJam > 1)
                {
                    biayaParkir = (selisihJam - 1) * biayaParkir;

                    Console.WriteLine("Biaya Parkir anda Rp." + biayaParkir);
                }
                else
                {
                    Console.WriteLine("Biaya Parkir anda Rp." + 200000);
                }
            }
            Console.WriteLine();
        }
    }
}
