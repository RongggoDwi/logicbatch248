﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class NaikGunung
    {
        public static void Resolve()
        {
            Console.WriteLine("Input Perjalanan Ke Puncak Gunung: ");
            string langkahKaki = Console.ReadLine();

            char[] langkahKakiArray = langkahKaki.ToCharArray();

            for (int k = 0; k < langkahKakiArray.Length; k++)
            {
                if (langkahKakiArray[k] == 'N' || langkahKakiArray[k] == 'n')
                {
                    langkahKakiArray[k] = 'U';
                }
                else if (langkahKakiArray[k] == 'T' || langkahKakiArray[k] == 't')
                {
                    langkahKakiArray[k] = 'D';
                }
            }

            int naikGunung = 0;
            int turunGunung = 0;
            int titikPendakian = 0;

            for (int i = 0; i < langkahKaki.Length; i++)
            {
                if (langkahKakiArray[i] == 'U' || langkahKakiArray[i] == 'u') // jika kondisi yang diinput U || u, untuk nyari berapa kali naik dari titik nol
                {
                    titikPendakian += 1;

                    if (titikPendakian == 1)
                    {
                        naikGunung++;
                    }
                }
                else if (langkahKakiArray[i] == 'D' || langkahKakiArray[i] == 'd') // jika kondisi yang diinput D || d, untuk nyari berapa kali turun dari titik nol
	            {
                    titikPendakian -= 1;

                    if (titikPendakian == -1)
                    {
                        turunGunung++;
                    }
	            }
            }
            
            Console.WriteLine("Output Total Up :" + naikGunung);
            Console.WriteLine("Output Total Down :" + turunGunung);
        }
    }
}
