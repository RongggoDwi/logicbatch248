﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class PembulatanNilai
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan angka ");
            string numbers = Console.ReadLine();

            int[] arrayNumbers = Utility.ConvertStringToIntArray(numbers);
            int[] arrayNumbersHasil = new int[arrayNumbers.Length];

            for (int i = 0; i < arrayNumbers.Length; i++)
            {
                if (arrayNumbers[i] < 100 && arrayNumbers[i] > 45)
                {
                    if (arrayNumbers[i] % 5 > 2)
                    {
                        arrayNumbersHasil[i] = (arrayNumbers[i] / 5) * 5 + 5;
                    }
                    else
                    {
                        arrayNumbersHasil[i] = (arrayNumbers[i] / 5) * 5;
                    }
                }
                else
                {
                    arrayNumbersHasil[i] = arrayNumbers[i];
                }

                Console.Write(arrayNumbersHasil[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
