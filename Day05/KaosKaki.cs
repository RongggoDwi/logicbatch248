﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class KaosKaki
    {
        public static void Resolve()
        {
            Console.WriteLine("Please input set of number");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            Array.Sort(numbersArray);

            int countSame = 0;

            for (int i = 0; i < (numbersArray.Length - 1);i++)
            {
                if (numbersArray[i] == numbersArray[i + 1])
                {
                    countSame += 1;
                    i++;
                }
            }

            int totalSameNumber = countSame;

            Console.WriteLine("The total pair of Number is " + totalSameNumber);

            Console.WriteLine();

        }
    }
}
