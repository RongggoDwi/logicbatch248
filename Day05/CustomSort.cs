﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class CustomSort
    {
        public static void Resolve()
        {
            //input = 2 1 3 5 2 1 4 output =1 1 2 2 3 4 5
            // tidak memakai short
            Console.WriteLine("Masukan angka yang akan diurutkan  ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int tukar;

            //Perulangan untuk geser angka terkecil ke paling kiri
            for (int i = 0; i < numbersArray.Length; i++)
            {
                //Perulangan untuk sorting dari angka terkecil -> terbesar
                for (int j = 1; j < numbersArray.Length; j++)
                {
                    //Kondisi untuk sorting descending
                    if (numbersArray[j] < numbersArray[j - 1])
                    {
                        tukar = numbersArray[j - 1];
                        numbersArray[j - 1] = numbersArray[j];
                        numbersArray[j] = tukar;
                    }
                }
            }
            //Perulangan untuk menampilkan hasil sorting
            for (int i = 0; i < numbersArray.Length; i++)
            {
                Console.Write(numbersArray[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
