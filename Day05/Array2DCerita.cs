﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05
{
    class Array2DCerita
    {
        public static void Resolve()
        {
            Console.WriteLine("Jumlah uang ");
            int uang = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan harga kacamata ");
            string kacaMata = Console.ReadLine();

            Console.WriteLine("Masukan harga baju ");
            string baju = Console.ReadLine();

            int[] kacamataArray = Utility.ConvertStringToIntArray1(kacaMata);

            int[] bajuArray = Utility.ConvertStringToIntArray2(baju);

            int temp = 0;
            int maximum = 0;

            for (int i = 0; i < kacamataArray.Length; i++)
            {
                for (int j = 0; j < bajuArray.Length; j++)
                {
                    if (kacamataArray[i] + bajuArray[j] <= uang)
                    {
                        temp = kacamataArray[i] + bajuArray[j];

                        if (temp < maximum)
                        {
                            maximum = temp;
                        }
                    }
                }
            }

            if (maximum == 0)
            {
                Console.WriteLine("Dana tidak mencukupi");
            }
            else
            {
                Console.WriteLine("Anda menggunakan uang anda sesuai maximum :" + maximum);
            }

        }
    }
}
