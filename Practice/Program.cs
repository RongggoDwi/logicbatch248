﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                string answer = "Y";

                while (answer.ToUpper() == "Y")
                {
                    Console.WriteLine("Masukan nomor soal");
                    int nomorSoal = int.Parse(Console.ReadLine());

                    switch (nomorSoal)
                    {
                        case 1:
                            Case01.Resolve();
                            break;

                        default:
                            Console.WriteLine("nomorSoal tidak ditemukan");
                            break;
                    }

                    Console.WriteLine("Lanjutkan?");
                    answer = Console.ReadLine();
                }
            }
        }
    }
}
