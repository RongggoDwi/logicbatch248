﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input nomor Soal");
            int nomorSoal = int.Parse(Console.ReadLine());
            if (nomorSoal == 1)
            {
                //Soal Penghitugan BMI

                Console.WriteLine("Masukan Berat Badan (satuan Kilogram): ");
                double berat1 = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukan tinggi Badan (satuan Meter): ");
                double tinggi1 = double.Parse(Console.ReadLine());

                double BMI = berat1 / (tinggi1 * tinggi1);

                Console.WriteLine();
                Console.WriteLine("BMI nya adalah: " + BMI);
                Console.WriteLine();

                if (BMI < 18.5)
                {
                    Console.WriteLine("Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Overweight");
                }
                else
                {
                    Console.WriteLine("Normal");
                }
                Console.ReadKey();
            }

            else if (nomorSoal == 2)
            {
                // Soal Deret satu dimensi LMS

                Console.WriteLine("Masukan Panjang angka");
                int length = int.Parse(Console.ReadLine());


                //Soal 01

                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                //memasukan value
                for (int i = 0; i < length; i++)
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil = angkaGanjil + 2;
                }

                // cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGanjilArray[i] + " ");
                }

                Console.WriteLine("\n");


                //Soal 02

                int[] angkaGenapArray = new int[length];
                int angkaGenap = 2;

                // masukan value
                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenap;
                    angkaGenap += 2;
                }

                // cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGenapArray[i] + " ");
                }
                Console.WriteLine("\n");


                //Soal 03

                int[] pertambahan3Array = new int[length];
                int pertambahan3 = 1;

                //masukan value
                for (int i = 0; i < length; i++)
                {
                    pertambahan3Array[i] = pertambahan3;
                    pertambahan3 = pertambahan3 + 3;
                }
                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(pertambahan3Array[i] + " ");
                }
                Console.WriteLine("\n");


                //Soal 04

                int[] pertambahan4Array = new int[length];
                int pertambahan4 = 1;

                //masukan value
                for (int i = 0; i < length; i++)
                {
                    pertambahan4Array[i] = pertambahan4;
                    pertambahan4 = pertambahan4 + 4;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(pertambahan4Array[i] + " ");
                }
                Console.WriteLine("\n");


                //SOAL 05

                int pertambahan5 = 1;
                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 == 0)
                    {
                        Console.Write("*" + " ");
                    }
                    else
                    {
                        Console.Write(pertambahan5 + " ");
                        pertambahan5 = pertambahan5 + 4;
                    }
                }
                Console.WriteLine("\n");


                //SOAL 06

                int pertambahan4Kedua = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("*" + " ");
                    }
                    else
                    {
                        Console.Write(pertambahan4Kedua + " ");
                    }
                    pertambahan4Kedua = pertambahan4Kedua + 4;
                }
                Console.WriteLine("\n");


                //SOAL 07

                int[] perkalian2Array = new int[length];
                int perkalian2 = 1;

                //masukan value
                for (int i = 0; i < length; i++)
                {
                    perkalian2Array[i] = perkalian2;
                    perkalian2 = perkalian2 * 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(perkalian2Array[i] + " ");
                }
                Console.WriteLine("\n");


                //SOAL 08

                int perkalian3 = 1;
                for (int i = 0; i < length; i++)
                {
                    perkalian3 = perkalian3 * 3;
                    Console.Write(perkalian3 + " ");
                }
                Console.WriteLine("\n");


                //SOAL 09

                int perkalian4 = 1;
                for (int i = 0; i < length; i++)
                {
                    if ((i + 1) % 3 != 0)
                    {
                        perkalian4 = perkalian4 * 4;
                        Console.Write(perkalian4 + " ");
                    }
                    else
                    {
                        Console.Write("*" + " ");
                    }
                }
                Console.WriteLine("\n");


                //SOAL 10

                int perkalian3Kedua = 1;
                for (int i = 1; i <= length; i++)
                {
                    perkalian3Kedua = perkalian3Kedua * 3;
                    if (i % 4 == 0)
                    {
                        Console.Write("XXX" + " ");
                    }
                    else
                    {
                        Console.Write(perkalian3Kedua + " ");
                    }
                }
                Console.WriteLine("\n");


                //SOAL 11

                int bilangan1 = 1;
                int bilangan2 = 0;
                int hasil = 0;


                //masukan velue
                for (int i = 0; i < length; i++)
                {
                    hasil = bilangan1 + bilangan2;
                    Console.Write(hasil + " ");
                    bilangan1 = bilangan2;
                    bilangan2 = hasil;
                }
                Console.WriteLine("\n");

                //SOAL 12

                int[] ganjilTerbalikArray = new int[length];
                int ganjilTerbalik = 1;

                //input value
                for (int i = 0; i < length; i++)
                {
                    // ganjil
                    if (length % 2 == 1)
                    {
                        if (i == 0) /*(i<1)*/
                        {
                            ganjilTerbalikArray[i] = 1;
                        }
                        else if (i <= (length / 2))
                        {
                            ganjilTerbalikArray[i] = ganjilTerbalikArray[i - 1] + 2;
                        }
                        else
                        {
                            ganjilTerbalikArray[i] = ganjilTerbalikArray[i - 1] - 2;
                        }
                    }

                    //genap
                    else
                    {
                        if (i < (length / 2))
                        {
                            ganjilTerbalikArray[i] = ganjilTerbalik;
                            ganjilTerbalik += 2;
                        }
                        else
                        {
                            ganjilTerbalik = ganjilTerbalik - 2;
                            ganjilTerbalikArray[i] = ganjilTerbalik;
                        }
                    }

                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(ganjilTerbalikArray[i] + " ");
                }
                Console.WriteLine("\n");

                //SOAL 13

                int bilangan01 = 1;
                int bilangan02 = 1;
                int bilangan03 = 1;
                int hasil1 = 0;


                //masukan velue
                for (int i = 0; i < length; i++)
                {
                    if (i <= 1)
                    {
                        hasil1 = bilangan01 + bilangan02 - 1 + bilangan03 - 1;
                        Console.Write(hasil1 + " ");
                    }
                    else
                    {
                        hasil1 = bilangan01 + bilangan02 + bilangan03;
                        Console.Write(hasil1 + " ");
                        bilangan01 = bilangan02;
                        bilangan02 = bilangan03;
                        bilangan03 = hasil1;
                    }

                }
                Console.WriteLine("\n");

                // SOAL 15

                int[] fibonacciTerbalikArray = new int[length];
                for (int i = 0; i < length; i++)
                // fibonacci
                {
                    if (i <= 1)
                    {
                        fibonacciTerbalikArray[i] = 1;
                    }
                    else 
                    {
                        fibonacciTerbalikArray[i] = fibonacciTerbalikArray[i - 1] + fibonacciTerbalikArray[i - 2];
                    }

                }

                // Cetak
                for (int i = 0; i < length; i++)
                {
                    Console.Write(fibonacciTerbalikArray[i] + " ");
                }
            }

            else
            {
                Console.WriteLine("Nomor Tidak ditemukan");
            }

            Console.ReadKey();
        }
    }
}
