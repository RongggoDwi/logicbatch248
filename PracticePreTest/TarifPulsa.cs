﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class TarifPulsa
    {
        public static void Resolve()
        {
            Console.WriteLine("Beli Pulsa Berapa");
            int pulsa = int.Parse(Console.ReadLine());

            int point = 0;

            while (pulsa > 0 && pulsa <= 10000)
            {
                pulsa = pulsa - 1000;
                point = 0;
            }
            while (pulsa > 10000 && pulsa < 30000)
            {
                if (pulsa - 1000 >= 0)
                {
                    pulsa = pulsa - 1000;
                    point++;
                }
            }
            while (pulsa > 30000)
            {
                if (pulsa - 1000 >= 0 )
                {
                    pulsa = pulsa - 1000;
                    point += 2;
                }

            }
            if (pulsa >= 30000)
            {
                Console.WriteLine("Total Point :" + (point+20));
            }
            else
            {
                Console.WriteLine("Total Point :" + point);
            }
            
        }
    }
}
