﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class Konversi
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Daftar Satuan");
            Console.WriteLine("Cangkir,Gelas,Teko,Botol");

            //input satuan awal
            Console.WriteLine("Masukan satuan awalnya : ");
            string satuanAwal = Console.ReadLine().ToLower();

            //input angka
            Console.WriteLine("Masukan angka yang akan dikonversi : ");
            int input = int.Parse(Console.ReadLine());

            //input satuan akhir
            Console.WriteLine("Masukkan satuan yang akan dikonversi : ");
            string satuanAkhir = Console.ReadLine().ToLower();

            double hasil = 0;

            //proses konversi
            if (satuanAwal == "botol" && satuanAkhir == "gelas")
            {
                hasil = input * 2.0;
            }
            else if (satuanAwal == "botol" && satuanAkhir == "cangkir")
            {
                hasil = input * 5.0;
            }
            else if (satuanAwal == "botol" && satuanAkhir == "teko")
            {
                hasil = input * 0.2;
            }
            else if (satuanAwal == "gelas" && satuanAkhir == "botol")
            {
                hasil = input * 0.5;
            }
            else if (satuanAwal == "gelas" && satuanAkhir == "cangkir")
            {
                hasil = input * 2.5;
            }
            else if (satuanAwal == "gelas" && satuanAkhir == "teko")
            {
                hasil = input * 0.1;
            }
            else if (satuanAwal == "teko" && satuanAkhir == "cangkir")
            {
                hasil = input * 25.0;
            }
            else if (satuanAwal == "teko" && satuanAkhir == "gelas")
            {
                hasil = input * 10.0;
            }
            else if (satuanAwal == "teko" && satuanAkhir == "botol")
            {
                hasil = input * 5.0;
            }
            else if (satuanAwal == "cangkir" && satuanAkhir == "gelas")
            {
                hasil = input * 0.4;
            }
            else if (satuanAwal == "cangkir" && satuanAkhir == "botol")
            {
                hasil = input * 0.2;
            }
            else if (satuanAwal == "cangkir" && satuanAkhir == "teko")
            {
                hasil = input * 0.04;
            }
            else
            {
                Console.WriteLine("Error");
            }

            //hasil konversi
            Console.WriteLine(input + " " + satuanAwal + " = " + hasil + " " + satuanAkhir);
        }
    }
}
