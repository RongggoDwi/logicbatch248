﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class Bensin
    {
        public static void Resolve()
        {
            Console.WriteLine("Perjalanan Ke");
            string perjalanan = Console.ReadLine().ToUpper();

            string[] splitJalan = perjalanan.Split('-');
            int jarak = 0;


            for (int i = 0; i < splitJalan.Length; i++)
            {
                if (splitJalan[i] == "TOKO")
                {
                    if (jarak > 0)
                    {
                        if (splitJalan[i - 1] == "TEMPAT4")
                        {
                            jarak = jarak + 6500;
                        }
                        else if (splitJalan[i - 1] == "TEMPAT3")
                        {
                            jarak = jarak + 4000;
                        }
                        else if (splitJalan[i - 1] == "TEMPAT2")
                        {
                            jarak = jarak + 2500;
                        }
                        else if (splitJalan[i - 1] == "TEMPAT1")
                        {
                            jarak = jarak + 2000;
                        }
                        else if (splitJalan[i - 1] == "TOKO")
                        {
                            jarak = jarak + 0;
                        }
                    }

                }
                else if (splitJalan[i] == "TEMPAT1")
                {
                    if (splitJalan[i - 1] == "TEMPAT1")
                    {
                        jarak = jarak + 0;
                    }
                    else if (splitJalan[i-1] == "TEMPAT2")
                    {
                        jarak = jarak + 500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT3")
                    {
                        jarak = jarak + 2000;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT4")
                    {
                        jarak = jarak + 4500;
                    }
                    else if (splitJalan[i - 1] == "TOKO")
                    {
                        jarak = jarak + 2000;
                    }
                }
                else if (splitJalan[i] == "TEMPAT2")
                {
                    if (splitJalan[i - 1] == "TEMPAT2")
                    {
                        jarak = jarak + 0;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT1")
                    {
                        jarak = jarak + 500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT3")
                    {
                        jarak = jarak + 1500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT4")
                    {
                        jarak = jarak + 4000;
                    }
                    else if (splitJalan[i] == "TOKO")
                    {
                        jarak = jarak + 2500;
                    }
                }
                else if (splitJalan[i] == "TEMPAT3")
                {
                    if (splitJalan[i - 1] == "TEMPAT2")
                    {
                        jarak = jarak + 1500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT1")
                    {
                        jarak = jarak + 2000;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT3")
                    {
                        jarak = jarak + 0;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT4")
                    {
                        jarak = jarak + 2500;
                    }
                    else if (splitJalan[i - 1] == "TOKO")
                    {
                        jarak = jarak + 4000;
                    }
                }
                else if (splitJalan[i] == "TEMPAT4")
                {
                    if (splitJalan[i - 1] == "TEMPAT2")
                    {
                        jarak = jarak + 4000;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT1")
                    {
                        jarak = jarak + 4500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT3")
                    {
                        jarak = jarak + 2500;
                    }
                    else if (splitJalan[i - 1] == "TEMPAT4")
                    {
                        jarak = jarak + 0;
                    }
                    else if (splitJalan[i - 1] == "TOKO")
                    {
                        jarak = jarak + 6500;
                    }
                }
            }
            double temp = Convert.ToDouble(jarak) / 1000;
            double bensin = 0;
            while (temp > 0)
            {
                if (temp >= 2.5)
                {
                    temp = temp - 2.5;
                    bensin++;
                }
                else if (temp < 2.5)
                {
                    bensin += temp * 0.4;
                    break;
                }
                
            }
            Console.WriteLine(bensin + " Liter Bensin");
        }
    }
}
