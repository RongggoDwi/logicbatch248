﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                string answer = "Y";

                while (answer.ToUpper() == "Y")
                {
                    Console.WriteLine("Masukan nomor soal");
                    int nomorSoal = int.Parse(Console.ReadLine());

                    switch (nomorSoal)
                    {
                        case 1:
                            Console.WriteLine("Soal 01");
                            TarifPulsa.Resolve();
                            break;
                        case 2:
                            Console.WriteLine("Soal 02");
                            Bensin.Resolve();
                            break;
                        case 3:
                            Console.WriteLine("Soal 03");
                            Konversi.Resolve();
                            break;
                        case 4:
                            Console.WriteLine("Soal 04");
                            Kopi.Resolve();
                            break;
                        case 5:
                            Console.WriteLine("Soal 05");
                            NasiGoreng.Resolve();
                            break;
                        case 6:
                            Console.WriteLine("Soal 06");
                            Permainan.Resolve();
                            break;
                        case 7:
                            Console.WriteLine("Soal 07");
                            KeranjangKosong.Resolve();
                            break;
                        case 8:
                            Console.WriteLine("Soal 08");
                            AntarBankAntarRekening.Resolve();
                            break;
                        case 9:
                            Console.WriteLine("Soal 09");
                            AntarBankAntarRekening.Resolve();
                            break;
                        case 10:
                            Console.WriteLine("Soal 10");
                            FibonacciPrima.Resolve();
                            break;
                        case 11:
                            Console.WriteLine("Soal 11");
                            NumberOne.Resolve();
                            break;
                        default:
                            Console.WriteLine("nomorSoal tidak ditemukan");
                            break;
                    }

                    Console.WriteLine("Lanjutkan?");
                    answer = Console.ReadLine();
                    Console.WriteLine();
                }
            }
        }
    }
}
