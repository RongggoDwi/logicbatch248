﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class AntarBankAntarRekening
    {
        public static void Resolve()
        {
            Console.WriteLine("Selamat datang di Mesin ATM");
            Console.WriteLine("Masukan PIN anda:");
            int pin = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int verivikasiPin = 123456;
            int biayaAdmin = 7500;

            if (pin != verivikasiPin)
            {
                Console.WriteLine("Mohon maaf PIN yang anda masukan SALAH");
            }
            else
            {
                Console.WriteLine("Masukan Uang yang ingin di Setor");
                int setorUang = int.Parse(Console.ReadLine());
                Console.WriteLine();

                Console.WriteLine("Pilihan Transfer:");
                Console.WriteLine("1.Antar Rekening || 2.Antar Bank");
                Console.WriteLine("Input 1 atau 2");
                int transferUang = int.Parse(Console.ReadLine());
                Console.WriteLine();

                if (transferUang == 1)
                {
                    Console.WriteLine("Masukan No rekening Tujuan");
                    string nomorRekening = Console.ReadLine();
                    char[] nomorChar = nomorRekening.ToCharArray();
                    Console.WriteLine();

                    if (nomorChar.Length > 10)
                    {
                        Console.WriteLine("Mohon maaf nomor rekening terlalu panjang");
                    }
                    else
                    {
                        Console.WriteLine("Masukan Nominal Transfer: ");
                        int nominalTransfer = int.Parse(Console.ReadLine());
                        setorUang = setorUang - nominalTransfer;
                        Console.WriteLine();

                        Console.WriteLine("Transaksi anda berhasil, saldo anda saat ini Rp." + setorUang);
                    }
                }
                else if (transferUang == 2)
                {
                    Console.WriteLine("Masukan kode Bank:");
                    string bankTujuan = Console.ReadLine();
                    Console.WriteLine();

                    Console.WriteLine("Masukan No rekening Tujuan");
                    string nomorRekening = Console.ReadLine();
                    char[] nomorChar = nomorRekening.ToCharArray();
                    Console.WriteLine();

                    if (nomorChar.Length > 10)
                    {
                        Console.WriteLine("Mohon maaf nomor rekening terlalu panjang");
                    }
                    else
                    {
                        Console.WriteLine("Masukan Nominal Transfer: ");
                        int nominalTransfer = int.Parse(Console.ReadLine());
                        setorUang = setorUang - nominalTransfer - biayaAdmin;
                        Console.WriteLine("Transaksi anda berhasil, saldo anda saat ini Rp." + setorUang);
                    }
                }
            }
        }
    }
}
