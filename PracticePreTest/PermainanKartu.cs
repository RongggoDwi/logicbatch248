﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticePreTest
{
    class PermainanKartu
    {
        public static void Resolve()
        {
            Console.WriteLine("Mari Mulai");
            Console.WriteLine("Jumlah kartu :");
            int kartu = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Masukan tawaran:");
            int tawaran = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine(" Anda sudah siap? (Y/N):");
            string answer = Console.ReadLine().ToUpper();

            while (answer.ToUpper() == "Y")
            {
                Random komputer = new Random();
                int kotakA = komputer.Next(0, 9);
                int kotakB = komputer.Next(0, 9);

                Console.WriteLine();
                Console.WriteLine("Silahkan pilih Kotak A atau B");
                Console.WriteLine("Input A atau B");

                string kotak = Console.ReadLine().ToUpper();
                if (kotak != "A" && kotak != "B")
                {
                    Console.WriteLine("Input anda salah");
                }

                else
                {

                    if (kotak == "A")
                    {
                        if (kotakB > kotakA)
                        {
                            kartu -= tawaran;
                        }
                        else if (kotakB < kotakA)
                        {
                            kartu += tawaran;
                        }
                    }

                    else if (kotak == "B")
                    {
                        if (kotakA > kotakB)
                        {
                            kartu -= tawaran;
                        }
                        else if (kotakA < kotakB)
                        {
                            kartu += tawaran;
                        }
                    }

                    if (kartu <= 0)
                    {
                        kartu = 0;
                        Console.WriteLine();
                        Console.WriteLine("YOU WINN");

                        Console.WriteLine();
                        Console.WriteLine("Kotak A :" + kotakA + " Kotak B :" + kotakB);
                        Console.WriteLine("Total Kartu anda : " + kartu);
                        break;

                    }
                    else
                    {
                        Console.WriteLine("Kotak A :" + kotakA + " Kotak B :" + kotakB);
                        Console.WriteLine("Total Kartu anda : " + kartu);


                        Console.WriteLine();
                        Console.WriteLine("Masih ingin lanjut? (Y/N):");
                        answer = Console.ReadLine().ToUpper();
                    }
                    if (answer.ToUpper() == "N")
                    {
                        Console.WriteLine("Anda Menyerah, YOU LOSSSE");
                    }

                }

            }
        }
    }
}
