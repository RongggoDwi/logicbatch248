﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class KertasA6
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan nilai input");
            string nilai = Console.ReadLine().ToUpper();

            string[] nilaiArray = new string[]{"A6", "A5", "A4", "A3", "A2", "A1", "A0" };
            int banyakCetak = 0;

            for (int i = 0; i < nilaiArray.Length; i++)
			{
                if (nilaiArray[i] == nilai)
	            {
                    banyakCetak = Convert.ToInt32(Math.Pow(2, i));
                    
            	}
			}
            Console.WriteLine("Banyak A6 Dibutuhkan :" + banyakCetak);
        }
    }
}
