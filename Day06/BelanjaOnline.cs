﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class BelanjaOnline
    {
        public static void Resolve()
        {
            Console.WriteLine("tanggal dan hari pemesanan");
            string pesan = Console.ReadLine();
            string[] SplitPesan = pesan.Split(' ');

            int tanggalPesan = int.Parse(SplitPesan[0]);

            Console.WriteLine("Hari libur Nasional");
            string libur = Console.ReadLine();

            string[] stringliburArray = libur.Split(' ');
            int[] liburArray = new int[stringliburArray.Length];

            for (int i = 0; i < liburArray.Length; i++)
            {
                liburArray[i] = Convert.ToInt32(stringliburArray[i]);
            }

            int lamaPengiriman = 7;
            int index = 0;
            string bulan = "";
            while (lamaPengiriman > 0)
            {
                if (liburArray[index] == tanggalPesan)
                {
                    if (index < liburArray.Length - 1)
                    {
                        index++;
                    }
                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                }
                else if (tanggalPesan != liburArray[index])
                {
                    if (tanggalPesan == 31)
                    {
                        tanggalPesan = 0;
                        bulan = " Bulan berikutnya";
                    }
                    lamaPengiriman--;
                }
                tanggalPesan++;
            }
            Console.WriteLine("Pesanan sampai pada tanggal " + tanggalPesan + bulan);
        }
    }
}
