﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class UrutanAbjad
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan kalimat");
            string kalimat = Console.ReadLine().ToLower();

            char[] kalimatArray = kalimat.ToCharArray();

            string pengecekan = "abcdefghijklmnopqrstuvwxyz ";
            char[] pengecekanAbjad = pengecekan.ToCharArray();

            string hurufVokal = "";
            string hurufKonsonan = "";

            for (int i = 0; i < kalimatArray.Length; i++)
			{
                for (int j = 0; j < pengecekanAbjad.Length; j++)
			    {
                    if (kalimatArray[i] == pengecekanAbjad[j])
	                {
                        if (kalimatArray[i] == 'a' || kalimatArray[i] == 'i' || kalimatArray[i] == 'u' || kalimatArray[i] == 'e' || kalimatArray[i] == 'o')
                    	{
                            hurufVokal = hurufVokal + kalimatArray[i];
	                    }
                        else
	                    {
                            hurufKonsonan = hurufKonsonan + kalimatArray[i];
	                    }
	                }
    			}
			}
            Console.WriteLine("Huruf Vokal :" + hurufVokal);
            Console.WriteLine("Huruf Konsonan :" + hurufKonsonan);
        }
    }
}
