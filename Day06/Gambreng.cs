﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Gambreng
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan Score A: ");
            string scoreA = Console.ReadLine().ToUpper();

            Console.WriteLine("Masukan Score B: ");
            string scoreB = Console.ReadLine().ToUpper();

            char[] scoreAChar = scoreA.ToCharArray();
            char[] scoreBChar = scoreB.ToCharArray();

            if (scoreAChar.Length != scoreBChar.Length)
            {
                Console.WriteLine("Inputan yang anda masukan salah");
            }
            else
            {
                int playerA = 0;
                int playerB = 0;

                int seri = 0;

                int kalahA = 0;
                int menangA = 0;

                int kalahB = 0;
                int menangB = 0;


                for (int i = 0; i < scoreAChar.Length; i++)
                {
                    if (scoreAChar[i] == scoreBChar[i])
                    {
                        seri++;
                    }
                    else if (i == i)
                    {
                        if (scoreAChar[i] == 'K' && scoreBChar[i] == 'B')
                        {
                            playerA++;
                            menangA++;
                            kalahB++;
                        }
                        else if (scoreAChar[i] == 'K' && scoreBChar[i] == 'G')
                        {
                            playerB++;
                            menangB++;
                            kalahA++;
                        }
                        else if (scoreAChar[i] == 'G' && scoreBChar[i] == 'K')
                        {
                            playerA++;
                            menangA++;
                            kalahB++;
                        }
                        else if (scoreAChar[i] == 'G' && scoreBChar[i] == 'B')
                        {
                            playerB++;
                            menangB++;
                            kalahA++;
                        }
                        else if (scoreAChar[i] == 'B' && scoreBChar[i] == 'K')
                        {
                            playerA++;
                            menangA++;
                            kalahB++;
                        }
                        else if (scoreAChar[i] == 'B' && scoreBChar[i] == 'G')
                        {
                            playerB++;
                            menangB++;
                            kalahA++;
                        }
                    }

                }

                Console.WriteLine("Player B :" + menangB + " Menang " + kalahB + " Kalah " + seri + " Seri");
                Console.WriteLine("Player A :" + menangA + " Menang " + kalahA + " Kalah " + seri + " Seri");

                Console.WriteLine();

                if (playerA > playerB)
                {
                    Console.WriteLine("Player A Menang");
                }
                else if (playerA < playerB)
                {
                    Console.WriteLine("Player B Menang");
                }
                else
                {
                    Console.WriteLine("Seri");
                }
            }



        }
    }
}

        
    

