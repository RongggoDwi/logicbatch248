﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class TarifParkir
    {
        public static void Resolve()
        {
            try
            {
                Console.WriteLine("Masukkan waktu masuk");
                string stringMasuk = Console.ReadLine();

                Console.WriteLine("Masukkan Waktu Keluar");
                string stringKeluar = Console.ReadLine();

                DateTime waktuMasuk = Convert.ToDateTime(stringMasuk);
                DateTime waktuKeluar = Convert.ToDateTime(stringKeluar);

                double selisihHari = (waktuKeluar - waktuMasuk).TotalDays;

                double lamaParkir = selisihHari * (double)24;

                double biaya = 0;

                if (lamaParkir <= 0)
                {
                    Console.WriteLine("input yang anda masukkan salah");
                }
                else if(lamaParkir < 8 && lamaParkir > 0)
                {
                    biaya = lamaParkir * 1000;
                    Console.WriteLine(biaya);
                }
                else if (lamaParkir > 8 && lamaParkir < 24)
                {
                    biaya = 8000;
                    Console.WriteLine(biaya);
                }
                else if ( lamaParkir > 24)
                {
                    double temp = lamaParkir;
                    int hari = 0;
                    while (temp > 24)
	                {
                        temp = Convert.ToInt32(temp) - 24;
                        hari++;
	                }
                    int parkirPerHari = 15000 * hari;
                    double sisaWaktu = lamaParkir - (hari * 24);

                    if (sisaWaktu <= 8)
	                {
                        biaya = sisaWaktu * 1000;
                        biaya = biaya - (biaya % 1000) + parkirPerHari;
	                }
                    else if (sisaWaktu < 8 && sisaWaktu < 24)
	                {
                        biaya = sisaWaktu * 1000;
                        biaya = biaya - (biaya % 1000) + 8000 + parkirPerHari;
	                }
                }
                Console.WriteLine("Biaya Parkir Anda adalah :" + biaya);
            }
            catch (Exception)
            {
                Console.WriteLine("Format Waktu Salah");   
            }
           

        }
    }
}
