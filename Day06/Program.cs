﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Kertas A6");
                        KertasA6.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("Tarif Parkir");
                        TarifParkir.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("Urutan Abjad");
                        UrutanAbjad.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("Belanja Online");
                        BelanjaOnline.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("Gambreng");
                        Gambreng.Resolve();
                        break;
                    //case 6:
                    //    Console.WriteLine("Array 2D Cerita");
                    //    Array2DCerita.Resolve();
                    //    break;
                    //case 7:
                    //    Console.WriteLine("Lilin Fibonacci");
                    //    LilinFibonacci.Resolve();
                    //    break;
                    //case 8:
                    //    Console.WriteLine("Int Geser");
                    //    IntGeser.Resolve();
                    //    break;
                    //case 9:
                    //    Console.WriteLine("Parkir");
                    //    Parkir.Resolve();
                    //    break;
                    //case 10:
                    //    Console.WriteLine("Naik Gunung");
                    //    NaikGunung.Resolve();
                    //    break;
                    //case 11:
                    //    Console.WriteLine("Kaos Kaki");
                    //    KaosKaki.Resolve();
                    //    break;
                    //case 12:
                    //    Console.WriteLine("Pembulatan Nilai");
                    //    PembulatanNilai.Resolve();
                    //    break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }
                Console.WriteLine("Lanjutkan ?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
