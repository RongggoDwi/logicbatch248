﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06
{
    class Utility
    {
        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            // CONVERT TO INT
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }
    }
}
