﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal04
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan nilai n");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan nilai n2");
            int n2 = int.Parse(Console.ReadLine());
            Console.WriteLine("\n");

            int[,] array2D = new int[2, n];

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else
                    {
                        // genap
                        if (j % 2 == 0)
                        {
                            if (j < 1)
                            {
                                array2D[i, j] = 1;
                            }
                            else
                            {
                                array2D[i, j] = array2D[i, j - 2] + 1;
                            }

                        }
                        // ganjil
                        else
                        {
                            if (j < 2)
                            {
                                array2D[i, j] = n2;
                            }
                            else
                            {
                                array2D[i, j] = array2D[i, j - 2] + n2;
                            }
                        }     
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}


                    
        
    

