﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02
{
    class Soal03
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan nilai n");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan nilai n2");
            int n2 = int.Parse(Console.ReadLine());
            Console.WriteLine("\n");

            int[,] array2D = new int[2, n];

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2D[i, j] = j;
                    }
                    else if (i == 1)
                    {
                        if (n % 2 == 1)
                        {
                            if (j <= n / 2)
                            {
                                array2D[i, j] = n2;
                                array2D[i, n - 1 - j] = n2;
                                n2 = n2 * 2;
                            }
                        }
                        else
                        {
                            if (j < n / 2)
                            {
                                array2D[i, j] = n2;
                                array2D[i, n - 1 - j] = n2;
                                n2 = n2 * 2;
                            }
                        }
                    }
                }
            }
            Utility.PrintArray2D(array2D);
        }
    }
}
