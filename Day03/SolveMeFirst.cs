﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SolveMeFirst
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the first number");
            int number1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter the second number");
            int number2 = int.Parse(Console.ReadLine());

            int total = Utility.sum(number1, number2);

            Console.WriteLine("Total is " + total);

        }
    }
}
