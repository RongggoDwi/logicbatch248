﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class DiagonalDifferents
    {
        public static void Resolve()
        {
            Console.WriteLine("Set the length of matrix");
            int length = int.Parse(Console.ReadLine());

            int[,] array2D = new int[length, length];
            int diagonal01 = 0;
            int diagonal02 = 0;
            int absolute = 0;


            // Untuk inputan angka Matrix
            for (int i = 0; i < length; i++)
            {
                Console.WriteLine("Enter the " + i + " set of numbers");
                String numbers = Console.ReadLine();

                int[] array = Utility.ConvertStringToIntArray(numbers);
                if (array.Length != length) 
                {
                    Console.WriteLine("Wrong Numbers, try again");
                    i = -1;
                }
                else
                {
                    for (int j = 0; j < length; j++)
                    {
                        array2D[i, j] = array[j];
                    }
                }
            }
            
            Console.WriteLine();

        // Diagonal Pertama
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (i == j)
                    {
                        Console.Write(array2D[i, j] + "\t");
                        diagonal01 += array2D[i, j];
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();
            Console.WriteLine(" Diagonal 1 : " + diagonal01);
            Console.WriteLine();

            // Diagonal Kedua
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if (i + j == length - 1)
                    {
                        Console.Write(array2D[i, j] + "\t");
                        diagonal02 += array2D[i, j];
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine();
            Console.WriteLine(" Diagonal 1 : " + diagonal02);
            absolute = Math.Abs(diagonal01 - diagonal02);
            Console.WriteLine("Nilai Absolute : " + absolute);
        }
    }
}
