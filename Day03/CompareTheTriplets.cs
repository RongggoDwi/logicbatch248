﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class CompareTheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the score of alice");
            string scoreAlice = Console.ReadLine();

            int[] aliceArray = Utility.ConvertStringToIntArray(scoreAlice);
            int totalAlice = 0;

            Console.WriteLine("Enter the score of bob");
            string scoreBob = Console.ReadLine();

            int[] bobArray = Utility.ConvertStringToIntArray(scoreBob);
            int totalBob = 0;

            if (aliceArray.Length != bobArray.Length)
            {
                Console.WriteLine("Format yang anda masukkan salah");
            }
            else
            {
                for (int i = 0; i < aliceArray.Length; i++)
                {
                    if (aliceArray[i] > bobArray[i])
                    {
                        totalAlice += 1;
                    }
                    else if (aliceArray[i] < bobArray[i])
                    {
                        totalBob += 1;
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Hasil Total Alice : " + totalAlice);
                Console.WriteLine("Hasil Total Bob : " + totalBob);
            }

            
        }
    }
}
