﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Utility
    {
        public static int sum(int number1, int number2)
        {

            int total = number1 + number2;
            return total;
        }

        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            // CONVERT TO INT
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }

        public static long[] ConvertStringToLong(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            long[] numbersArray = new long[stringNumbersArray.Length];

            // CONVERT TO INT
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = long.Parse(stringNumbersArray[i]);
            }

            return numbersArray;
        }

        //public static int[] dateTimeToIntArray(string numbers)
        //{
        //    string[] stringNumbersArray = numbers.Split(':');
        //    int[] numbersArray = new int[stringNumbersArray.Length];

        //    // CONVERT TO INT
        //    for (int i = 0; i < numbersArray.Length; i++)
        //    {
        //        numbersArray[i] = int.Parse(stringNumbersArray[i]);
        //    }

        //    return numbersArray;
        //}

        //public static void PrintArray2D(int[,] array2D)
        //{
        //    for (int i = 0; i < array2D.GetLength(0); i++)
        //    {
        //        for (int j = 0; j < array2D.GetLength(1); j++)
        //        {
        //            Console.Write(array2D[i, j] + " ");
        //        }
        //        Console.WriteLine();
        //    }
        //}
    }
}
