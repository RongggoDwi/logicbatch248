﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Mini_Max_Sum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int total = 0;
            int minimum = 0;
            int maximum = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                total += numbersArray[i];
            }

            minimum = total - numbersArray.Max();
            maximum = total - numbersArray.Min();
            Console.WriteLine(minimum.ToString() + " " + maximum.ToString());
        }
    }
}
