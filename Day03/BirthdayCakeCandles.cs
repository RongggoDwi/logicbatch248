﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class BirthdayCakeCandles
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int candleTallest = numbersArray.Max();
            int candle = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] == candleTallest)
                {
                    candle += 1;
                }
                
            }

            Console.WriteLine("\n" + candle);
        }
    }
}
