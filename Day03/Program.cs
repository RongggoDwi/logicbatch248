﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukan nomor soal : ");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 1:
                        Console.WriteLine("Solve me first");
                        SolveMeFirst.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Time conversion");
                        TimeConversion.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Simple array sum");
                        SimpleArraySum.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Diagonal differents");
                        DiagonalDifferents.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Plus Minus");
                        PlusMinus.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Staircase");
                        Staircase.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("MiniMaxSum");
                        Mini_Max_Sum.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Birtday Cake Candles");
                        BirthdayCakeCandles.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("A Very Big Sum");
                        AVeryBigSum.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Compare The Triplets");
                        CompareTheTriplets.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
                Console.WriteLine();
            }
        }
    }
}
