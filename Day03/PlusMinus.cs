﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int positifNumber = 0;
            int negatifNumber = 0;
            int zeroNumber = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                  if (numbersArray[i] > 0)
                  {
                      ++positifNumber;
                  }
                  else if (numbersArray[i] < 0)
                  {
                      ++negatifNumber;
                  }
                  else
                  {
                      zeroNumber++;
                  }
            }

            Console.WriteLine("result positif number =  " + (double)positifNumber / numbersArray.Length);
            Console.WriteLine("result negatif number =  " + (double)negatifNumber / numbersArray.Length);
            Console.WriteLine("result zero number =  " + (double)zeroNumber / numbersArray.Length);

        }
    }
}
