﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class TimeConversion
    {
        public static void Resolve()
        {
            Console.WriteLine("Input the time");
            string time = Console.ReadLine();

            if (!time.Contains("AM") && !time.Contains("PM"))
            {
                Console.WriteLine("Wrong format");
            }
            else
            {
                try
                {
                    DateTime dateTime = Convert.ToDateTime(time);
                    Console.Write("Format 24 jam : ");
                    Console.WriteLine(dateTime.ToString("HH:mm:ss"));
                }
                catch (Exception)
                {
                    Console.WriteLine("Format waktu yang anda masukkan salah!");
                }
            }

        }
    }
}

