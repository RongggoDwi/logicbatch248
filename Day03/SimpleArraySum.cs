﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class SimpleArraySum
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the set of number");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int total = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                    total += numbersArray[i];

            }
            Console.WriteLine("Result is " + total);
        }
    }
}
